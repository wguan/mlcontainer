#----------------------------------------------------------------
# Generated CMake target import file.
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "IRT" for configuration ""
set_property(TARGET IRT APPEND PROPERTY IMPORTED_CONFIGURATIONS NOCONFIG)
set_target_properties(IRT PROPERTIES
  IMPORTED_LOCATION_NOCONFIG "${_IMPORT_PREFIX}/lib/libIRT.so"
  IMPORTED_SONAME_NOCONFIG "libIRT.so"
  )

list(APPEND _cmake_import_check_targets IRT )
list(APPEND _cmake_import_check_files_for_IRT "${_IMPORT_PREFIX}/lib/libIRT.so" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
