#!/bin/sh

CurrentDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

export DETECTOR=epic
# export DETECTOR_PATH=/afs/cern.ch/user/w/wguan/workdisk/eic/dRICH-MOBO/MOBO-tools/eic-software/share/epic
export DETECTOR_PATH=${CurrentDir}/share/epic
export DETECTOR_CONFIG=${1:-epic}

## Warn is not the right name (this script is sourced, hence $1)
if [[ "$(basename ${BASH_SOURCE[0]})" != "thisepic.sh" ]]; then
        echo "Warning: This script will cease to exist at '$(realpath --no-symlinks ${BASH_SOURCE[0]})'."
        echo "         Please use the version at '$(realpath --no-symlinks $(dirname ${BASH_SOURCE[0]})/bin/thisepic.sh)'."
fi

## Export detector libraries
if [[ "$(uname -s)" = "Darwin" ]] || [[ "$OSTYPE" == "darwin"* ]]; then
        # export DYLD_LIBRARY_PATH="/afs/cern.ch/user/w/wguan/workdisk/eic/dRICH-MOBO/MOBO-tools/eic-software/lib${DYLD_LIBRARY_PATH:+:$DYLD_LIBRARY_PATH}"
        export DYLD_LIBRARY_PATH="${CurrentDir}/lib${DYLD_LIBRARY_PATH:+:$DYLD_LIBRARY_PATH}"
else
        # export LD_LIBRARY_PATH="/afs/cern.ch/user/w/wguan/workdisk/eic/dRICH-MOBO/MOBO-tools/eic-software/lib${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH}"
	export LD_LIBRARY_PATH="${CurrentDir}/lib${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH}"
fi
