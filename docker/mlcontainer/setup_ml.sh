#!/bin/bash

# source /opt/miniconda3/etc/profile.d/conda.sh
# conda activate /opt/ml

myName="${BASH_SOURCE:-$0}"
myDir=$(dirname $myName)
myDir=$(readlink -f -- $myDir)

source ${myDir}/miniconda3/etc/profile.d/conda.sh
conda activate ${myDir}/ml
