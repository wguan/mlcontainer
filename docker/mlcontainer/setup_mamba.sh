#!/bin/bash

myName="${BASH_SOURCE:-$0}"
myDir=$(dirname $myName)
myDir=$(readlink -f -- $myDir)

export MAMBA_EXE=$myDir/bin/micromamba

shellName=$(readlink /proc/$$/exe | awk -F "[/-]" '{print $NF}')
typeset -f micromamba >/dev/null || eval "$($MAMBA_EXE shell hook --shell=$shellName)"

# activate the env
CondaDir=/opt/conda
export CONDA_PREFIX=$myDir${CondaDir}
if [ ! -d $CONDA_PREFIX ]; then
   CondaDir=""
fi
export CONDA_PREFIX=$myDir${CondaDir}
export MAMBA_ROOT_PREFIX=$CONDA_PREFIX

micromamba activate

unset PYTHONHOME
